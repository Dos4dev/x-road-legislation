# X-Road-Legislation

This repository was created to collect X-Road related legistation in asciidoc format.
The repository is follow [`documentation as code`](https://docs-as-co.de) and `law as code` philosophies.
Having a hyperlink for each part of the law is a major advantage of this project. 
The sources of the repository are availbale at the following [address](https://dos4dev.gitlab.io/xroad-docs).
